// import HouseHurley from './../../assets/model/house.glb';
// import HouseHurley from './../../assets/model/house_obj_UVs_tex (1).glb';
import HouseHurley from './../../assets/model/optimized_house.glb';
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {DRACOLoader} from "three/examples/jsm/loaders/DRACOLoader";

const castShadowMeshList = ['Slab', 'Wall_main_outside'];
const receiveShadowMeshList = ['ground', 'Floor', 'Wall_main_inside'];

export default class Loader {
    constructor(viewer) {

    }

    loadModel(scene) {
        const loader = new GLTFLoader();
        const dracoLoader = new DRACOLoader();
        dracoLoader.setDecoderPath( 'draco/' );
        loader.setDRACOLoader( dracoLoader );
        loader.load(HouseHurley, (gltfModel) => {

            console.log('gltfModel', gltfModel);

            const model = gltfModel.scene;

            // model.scale.multiplyScalar(0.06);
            model.traverse(el => {
                if (el.isMesh) {

                    // el.visible = false;

                    if (castShadowMeshList.includes(el.name)) {
                        el.visible = true;
                        el.castShadow = true;
                    }
                    if (receiveShadowMeshList.includes(el.name)) {
                        el.visible = true;
                        el.receiveShadow = true;
                    }
                    if (el.name === 'Wall_main_outside') {
                        el.visible = true;
                        el.material.side = THREE.DoubleSide;
                    }
                    // el.castShadow = true;
                    // el.receiveShadow = true;

                    // if (el.material.length) {
                    //     for (const material of el.material) material.side = THREE.DoubleSide;
                    // }
                    // else material.side = THREE.DoubleSide;
                }
            });

            scene.add( model );

        },
            ( xhr ) => {

                console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

            },
            // called when loading has errors
            ( error ) => {

                console.log( 'An error happened' );

            });
    }
}
