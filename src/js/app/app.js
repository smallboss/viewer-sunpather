import Viewer from '../viewer/viewer';
import AppBuilder from "./AppBuilder/AppBuilder";

export default class App{
    constructor(){
        this.viewer = new Viewer();
        this.appBuilder = new AppBuilder(this.viewer);
        this.appBuilder.init();
    }

}
